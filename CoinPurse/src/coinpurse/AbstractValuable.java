package coinpurse;

/**
 * Abstract class for Valuables to extends from this class.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.2.10
 */
public abstract class AbstractValuable implements Valuable {

	public AbstractValuable() {
		super();
	}

	/**
	 * Compare Valuable's by value. They are equal if their values are same.
	 * 
	 * @param other
	 *            is another Coin to compare to this one.
	 * @return true if the value is same, false otherwise.
	 */
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (other.getClass() != this.getClass()) {
			return false;
		}
		if (((Valuable) other).getValue() == this.getValue()) {
			return true;
		}
		return false;
	}

	/**
	 * Compare another Valuable with this Valuable.
	 * 
	 * @param other
	 *            is another Valuable to be compared with this one.
	 * @return difference in value of two Valuables (this Valuable's value
	 *         subtracted by other Valuable's value).
	 */
	public int compareTo(Valuable other) {
		return (int) Math.signum(this.getValue() - other.getValue());
	}
}
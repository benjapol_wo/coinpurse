package coinpurse;

/**
 * A bank note with a value and a serial number starting from 1000000
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.2.10
 */
public class BankNote extends AbstractValuable {
	/** Static serial number runner */
	private static long serializer = 1000000;

	/** Value of this BankNote */
	private double value;
	/** Serial number of this BankNote */
	private long serialNumber;

	/**
	 * Create a new BankNote with received value and a serial number from
	 * serializer. Then increment serializer value by 1.
	 * 
	 * @param value
	 *            is the value for the new BankNote
	 */
	public BankNote(double value) {
		this.value = value;
		serialNumber = serializer++;
	}

	/**
	 * Get a real value of this bank note in currency
	 * 
	 * @return a real value of this bank note in currency
	 */
	public double getValue() {
		return value;
	}

	/**
	 * Get a String representation of this bank note
	 * 
	 * @return a String representation of this bank note - including serial
	 *         number
	 */
	public String toString() {
		return "a THB " + value + " bank note. (S/N: " + serialNumber + ")";
	}
}

package coinpurse;

/**
 * A coin with a monetary value. You can't change the value of a coin.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.2.10
 */
public class Coin extends AbstractValuable {

	/** Value of the coin */
	private double value;

	/**
	 * Constructor for a new coin.
	 * 
	 * @param value
	 *            is the value for the coin
	 */
	public Coin(double value) {
		this.value = value;
	}

	/**
	 * Return the value of a coin.
	 * 
	 * @return value of a coin.
	 */
	@Override
	public double getValue() {
		return this.value;
	}

	/**
	 * Give a String representation of the coin.
	 * 
	 * @return a String representation of the coin.
	 */
	public String toString() {
		return "a THB " + value + " coin.";
	}

}
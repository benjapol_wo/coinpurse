package coinpurse;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;
import java.util.Set;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.OverWithdraw;
import coinpurse.strategy.RecursiveWithdraw;

/**
 * User Interface for a Coin Purse that accepts Coin, Coupon, and BankNote
 * objects. To compile this, you need Coin with a Coin(double value) constructor
 * BankNote with a BankNote(int value) constructor Coupon with a Coupon(String
 * color) constructor
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.4
 */
public class ConsoleDialog {

	// create a java.util.Scanner object for use in all methods
	private static Scanner console = new Scanner(System.in);
	private static String currency = "Baht";
	private Purse purse;

	/**
	 * Initialize a new user interface.
	 * 
	 * @param purse
	 *            is the Purse to communicate with.
	 */
	public ConsoleDialog(Purse purse) {
		this.purse = purse;
	}

	/** run the user interface */
	public void run() {

		while (true) {
			System.out.println("Purse contains " + purse.getBalance() + " "
					+ currency);
			if (purse.isFull())
				System.out.println("Purse is FULL.");

			// print a list of choices
			System.out
					.print("\nPlease enter d (deposit), w (withdraw), p (print), or q (quit): ");
			String choice = console.next();
			String args = console.nextLine().trim();
			// TODO what if user inputs uppercase "D" or "W"? Fix this.
			if (choice.equalsIgnoreCase("d"))
				depositDialog(args);
			else if (choice.equalsIgnoreCase("w"))
				withdrawDialog(args);
			else if (choice.equalsIgnoreCase("p"))
				printDialog();
			else if (choice.equalsIgnoreCase("c")) {
				changeWithdrawStr(args);
			} else if (choice.equalsIgnoreCase("q"))
				break; // leave the loop
			else
				System.out.println("\"" + choice + "\" is not a valid choice.");
		}
		// confirm that we are quitting
		System.out.printf("Goodbye. The bank keeps %f %s\n",
				purse.getBalance(), currency);
	}

	/**
	 * Ask the user for new WIthdrawStrategy and change to use that strategy for
	 * withdrawals.
	 */
	private void changeWithdrawStr(String args) {
		if (args.length() == 0) {
			System.out.println("Available Strategies :");
			System.out.println(" 1) Greedy's Algorithm");
			System.out.println(" 2) Recursive Withdraw");
			System.out.println(" 3) Over Withdraw");
			System.out.print("Pick one -> ");
			args = console.next();
			System.out.println();
		}
		switch (args.charAt(0)) {
		case '1':
			purse.setWithdrawStrategy(new GreedyWithdraw());
			System.out.println("Strategy changed to Greedy's Algorithm");
			break;
		case '2':
			purse.setWithdrawStrategy(new RecursiveWithdraw());
			System.out.println("Strategy changed to Recursive Withdraw");
			break;
		case '3':
			purse.setWithdrawStrategy(new OverWithdraw());
			System.out.println("Strategy changed to Over Withdraw");
			break;
		default:
			System.out.println("Invalid straregy choice");
		}

	}

	/**
	 * Ask the user what money to deposit, then deposit them. Show result of
	 * success or failure.
	 */
	public void depositDialog(String args) {
		if (args.length() == 0) {
			System.out.print("Enter the values to deposit [eg: 5 blue 100]: ");
			args = console.nextLine();
		}
		// parse the input line into numbers
		Scanner scanline = new Scanner(args);
		Valuable money = null;
		while (scanline.hasNext()) {
			money = MoneyFactory.getInstance().makeMoney(scanline.next());
			if (money == null) {
				continue;
			}
			System.out.printf("Deposit %s... ", money.toString());
			boolean ok = purse.insert(money);
			System.out.println((ok ? "ok" : "FAILED"));
		}
		scanline.close();
	}

	/**
	 * Ask how much money to withdraw and then do it. After withdraw, show the
	 * values of the coins we withdrew.
	 */
	public void withdrawDialog(String args) {
		if (args.length() == 0) {
			System.out.print("How much to withdraw? ");
			args = console.nextLine();
		}
		Scanner scanline = new Scanner(args);
		if (scanline.hasNextDouble()) {
			double amount = scanline.nextDouble();
			Object[] array = purse.withdraw(amount);
			if (array == null)
				System.out.printf("Sorry, couldn't withdraw %f Baht\n", amount);
			else {
				System.out.print("You withdrew:");
				String space = " ";
				for (int k = 0; k < array.length; k++) {
					System.out.print(space + array[k].toString());
				}
				System.out.println();
			}
		} else
			System.out.printf("Invalid amount.");
		scanline.close();
	}

	/**
	 * print contents of purse.
	 */
	public void printDialog() {
		System.out.println(purse.toString());
	}

	/**
	 * Save purse state to a file using serialization.
	 * 
	 * @param purse
	 *            is the Purse to save
	 * @param filename
	 *            is the output file. Contents will be overwritten.
	 */
	public static void savePurse(Purse purse, String filename) {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(filename);
			ObjectOutputStream xout = new ObjectOutputStream(out);
			xout.writeObject(purse);
			xout.flush();
			System.out.println("Purse saved to " + filename);
			xout.close();
		} catch (IOException ioe) {
			System.out.println("Error saving purse: " + ioe.getMessage());
		} finally {
			if (out != null)
				try {
					out.close();
				} catch (IOException ex) { /* ignore */
				}
		}
	}

	public static Purse loadPurse(String filename) {
		Purse purse = null;
		InputStream in = null;
		try {
			File file = new File(filename);
			if (!file.exists())
				return null;
			in = new FileInputStream(file);
			ObjectInputStream xin = new ObjectInputStream(in);
			Object obj = xin.readObject();
			if (obj instanceof Purse)
				purse = (Purse) obj;
			else
				System.out.println(filename + " contains a "
						+ obj.getClass().getName());
			xin.close();
		} catch (IOException ioe) {
			System.out.println("Error loading purse: " + ioe.getMessage());
		} catch (ClassNotFoundException cne) {
			System.out
					.println(filename
							+ " contains unrecognized object data. ClassNotFoundException.");
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException ex) { /* ignore */
				}
		}
		return purse;
	}

	public static void main(String[] args) {
		System.out.println("Purse by Ralph Lauren.");
		Purse purse = loadPurse("purse.dat");
		if (purse == null)
			purse = new Purse(20);
		ConsoleDialog console = new ConsoleDialog(purse);
		console.run();
		savePurse(purse, "purse.dat");
	}
}

/**
 * Encapsulate operation of creating money objects.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.4
 */
class MoneyFactory {
	/** An instance of MoneyFactory. */
	private static MoneyFactory instance = new MoneyFactory();
	/** Set of known Coupon colors. */
	private static Set<String> knownColors = Coupon.getColors();

	/**
	 * Create a new instance of MoneyFactory.
	 */
	private MoneyFactory() {

	}

	/**
	 * Get the money factory instance.
	 */
	public static MoneyFactory getInstance() {
		return instance;
	}

	/**
	 * Return a money object that represents the value of the String parameter.
	 * 
	 * @param amount
	 *            is amount of money or some description, e.g. "red" for coupon
	 * @return Valuable object for the amount.
	 */
	public Valuable makeMoney(String amount) {
		try {
			double amt = Double.valueOf(amount);
			if (amt < 20)
				return new Coin(amt);
			return new BankNote((int) amt);

		} catch (NumberFormatException e) {
			if (knownColors.contains(amount.trim().toLowerCase())) {
				return new Coupon(amount);
			}
			System.out.println("unknown color for coupon: " + amount);
			return null;
		}
	}
}

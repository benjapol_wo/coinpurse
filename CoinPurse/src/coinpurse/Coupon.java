package coinpurse;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A coupon with color-value key-value pairs.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.4
 */
public class Coupon extends AbstractValuable {
	/** Color of this Coupon */
	private String color;
	/** Value of each Coupon color. */
	private static Map<String, Double> colorValue = new HashMap<String, Double>();

	static {
		colorValue = new HashMap<String, Double>();
		colorValue.put("red", 100.0);
		colorValue.put("blue", 50.0);
		colorValue.put("green", 20.0);
	}

	/**
	 * Create a new coupon with received color. Will always convert the color
	 * String's first character into UpperCase for easier management of
	 * key-value pairs. Also initializes the key-vale pairs to predefined
	 * settings
	 * 
	 * @param color
	 *            is the color of the new coupon
	 */
	public Coupon(String color) {
		this.color = color.toLowerCase();
	}

	/**
	 * Get the real value of the new coupon in currency
	 * 
	 * @return value of the coupon in currency
	 */
	public double getValue() {
		return colorValue.get(color);
	}

	/**
	 * Get the Set that contain all Coupon color(s).
	 * 
	 * @return The Set that contain all Coupon color(s).
	 */
	public static Set<String> getColors() {
		return colorValue.keySet();
	}

	/**
	 * Get a String representation of this coupon
	 * 
	 * @return A String representation of this coupon.
	 */
	public String toString() {
		return "a " + color + " (THB " + colorValue.get(color) + ") coupon.";
	}

}

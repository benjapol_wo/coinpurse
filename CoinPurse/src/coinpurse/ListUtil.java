package coinpurse;

import java.util.Arrays;
import java.util.List;

/**
 * This class was used to practice recursive.
 * 
 * @author Benjapol Worakan 5710546577
 * @version EXPERIMENTAL 15.3.2a
 */
public class ListUtil {
	/**
	 * Sum the value of all elements in an array of double recursively.
	 * 
	 * @param x
	 *            - an array to get sum
	 * @return a sum of the given array.
	 */
	public static double sum(double[] x) {
		return sumTo(x, x.length - 1);
	}

	/**
	 * HELPER METHOD
	 * 
	 * @param x
	 *            - array of elements to sum
	 * @param lastIndex
	 *            - index of the last element to sum
	 */
	private static double sumTo(double[] x, int lastIndex) {
		if (lastIndex < 0) {
			return 0;
		}
		return x[lastIndex] + sumTo(x, lastIndex - 1);
	}

	/**
	 * Find a String with highest lexical value from a List of String(s)
	 * recursively.
	 * 
	 * @param list
	 *            - a list of String
	 * @return a String with highest lexical value the list.
	 */
	public static String max(List<String> list) {
		if (list.size() == 1) {
			return list.get(0);
		}
		String max = max(list.subList(1, list.size()));
		if (list.get(0).compareTo(max) > 0) {
			return list.get(0);
		}
		return max;
	}

	/**
	 * Print out all elements of a List recursively.
	 * 
	 * @param list
	 *            - a List to be printed out
	 */
	public static void printList(List<?> list) {
		if (list.size() <= 0) {
			return;
		}
		System.out.print(list.get(0).toString());
		if (list.size() == 1) {
			System.out.println();
			return;
		}
		System.out.print(", ");
		printList(list.subList(1, list.size()));
	}

	/**
	 * This method runs this class.
	 * 
	 * @param args
	 *            - not used
	 */
	public static void main(String[] args) {
		System.out.println(ListUtil.sum(new double[] { 1.0, 2.0, 3.0, 4.0 }));
		List<String> food = Arrays.asList("apple", "banana", "grape", "fig");
		printList(food);
		List<String> list;
		if (args.length > 0) {
			list = Arrays.asList(args);
		} else {
			list = Arrays.asList("bird", "zebra", "cat", "pig");
		}

		System.out.print("List contains: ");
		printList(list);

		System.out.println("Lexically greatest element is " + max(list));
	}
}

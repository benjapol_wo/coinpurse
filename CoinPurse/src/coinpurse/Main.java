package coinpurse;

/**
 * A main class to create objects and connect objects together. The user
 * interface needs a reference to purse.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.4
 */
public class Main {

	/**
	 * Initialize a purse, add a PurseObserver to it, and run a new
	 * ConsoleDialog.
	 * 
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		Purse purse = new Purse(10);
		purse.addObserver(new PurseObserver());

		new ConsoleDialog(purse).run();
	}
}

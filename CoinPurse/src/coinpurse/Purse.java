package coinpurse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;

import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;

/**
 * A purse contains Valuable(s). You can insert/withdraw Valuables, check the
 * balance, and check if the purse is full. When you withdraw money, the purse
 * decides which Valuable(s) to remove using the predefined WithdrawStrategy.
 * 
 * This Purse also inherited from Observable, which will make it display its
 * balance and status via GUI using PurseObserver, if supplied.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.2.25
 */
public class Purse extends Observable {

	/**
	 * Withdrawal strategy that will be used when withdraw method is called. The
	 * default strategy is RecursiveWithdraw.
	 */
	private WithdrawStrategy withdrawStrategy;

	/** Collection of Valuable(s) in the Purse. */
	private ArrayList<Valuable> valuableList;

	/**
	 * Capacity is maximum number of Valuable(s) the Purse can hold. Capacity is
	 * set when the Purse is created.
	 */
	private int capacity;

	/**
	 * Create a Purse with a specified capacity. Also defaults withdrawStrategy
	 * to new RecursiveWithdraw().
	 * 
	 * @param capacity
	 *            - maximum number of Valuable(s) that can be stored in the
	 *            Purse.
	 */
	public Purse(int capacity) {
		this.valuableList = new ArrayList<Valuable>();
		this.capacity = capacity;
		this.withdrawStrategy = new RecursiveWithdraw();
	}

	/**
	 * Count and return the number of Valuable(s) in the Purse. This is the
	 * number of Valuable(s), not their value.
	 * 
	 * @return the number of Valuables in the Purse.
	 */
	public int count() {
		return valuableList.size();
	}

	/**
	 * Get the total value of all items in the Purse.
	 * 
	 * @return the total value of items in the Purse.
	 */
	public double getBalance() {
		double totalValue = 0;
		for (Valuable val : valuableList) {
			totalValue += val.getValue();
		}
		return totalValue;
	}

	/**
	 * Return the capacity of the Valuables Purse.
	 * 
	 * @return the capacity of the Purse.
	 */
	public int getCapacity() {
		return capacity;
	}

	/**
	 * Re-set the Purse's WithdrawStrategy.
	 * 
	 * @param strategy
	 *            - new WithdrawStrategy for the Purse
	 */
	public void setWithdrawStrategy(WithdrawStrategy strategy) {
		this.withdrawStrategy = strategy;
	}

	/**
	 * Test whether the Purse is full. The Purse is full if number of items in
	 * Purse is equal to or greater than the Purse capacity.
	 * 
	 * @return true if Purse is full.
	 */
	public boolean isFull() {
		if (valuableList.size() >= capacity) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Insert a Valuable into the Purse. The Valuable is only inserted if the
	 * Purse has space for it and the Valuable has positive value.
	 * 
	 * This method also alert the Observer that the Purse has been modified.
	 * 
	 * @param valuable
	 *            - a Valuable object to be inserted into the Purse
	 * @return true if Valuable is inserted, false if can't insert.
	 */
	public boolean insert(Valuable valuable) {
		if (!isFull() && valuable.getValue() > 0) {
			valuableList.add(valuable);
			setChanged();
			notifyObservers();
			return true;
		}
		return false;
	}

	/**
	 * Withdraw the requested amount of money. Return an array of Valuables
	 * withdrawn from the Purse, or return null if cannot withdraw the amount
	 * requested.
	 * 
	 * This method also alert the Observer that the Purse has been modified.
	 * 
	 * @param amount
	 *            - the amount to withdraw
	 * @return array of Valuable objects for money withdrawn, or null if cannot
	 *         withdraw requested amount.
	 */
	public Valuable[] withdraw(double amount) {
		if (amount < 0) {
			return null;
		}

		Valuable[] withdrawArray = withdrawStrategy.withdraw(amount,
				new ArrayList<Valuable>(valuableList));

		if (withdrawArray != null) {
			for (Valuable val : withdrawArray) {
				valuableList.remove(val);
			}
			setChanged();
			notifyObservers();
		}

		return withdrawArray;
	}

	/**
	 * Returns a string description of the Purse's contents.
	 * 
	 * @return a String representation of the Purse's contents.
	 */
	public String toString() {
		return String.format("%s\n%s", "A purse with " + valuableList.size()
				+ " items. With a total value of THB " + getBalance() + ".",
				"Items : " + Arrays.toString(valuableList.toArray()));
	}
}
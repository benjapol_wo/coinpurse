package coinpurse;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

/**
 * A GUI observer for a Purse. Shows balance and remaining capacity of the Purse
 * in two separated windows (JFrame).
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.4
 */
public class PurseObserver implements Observer {

	/** JFrame windows for displaying the Purse's balance and status. */
	private JFrame balanceFrame, statusFrame;

	/** Labels for displaying the Purse's balance and status. */
	private JLabel balanceLabel, statusLabel;

	/** A progress bar that displays the remaining capacity of the Purse. */
	private JProgressBar statusBar;

	/**
	 * Create and initialize the PurseObserver.
	 */
	public PurseObserver() {
		initUIComponents();
		balanceFrame.setVisible(true);
		statusFrame.setVisible(true);
	}

	/**
	 * Update the GUI components using the information from Purse.
	 * 
	 * @param purse
	 *            - a Purse that is the source of the information
	 * @param optArg
	 *            - optional arguments that is from notifyObservers() method,
	 *            specified by the Observer interface, but not used here
	 */
	@Override
	public void update(Observable purse, Object optArg) {
		if (purse instanceof Purse) {
			balanceLabel.setText(Double.toString(((Purse) purse).getBalance()));
			statusBar.setMaximum(((Purse) purse).getCapacity());
			statusBar.setValue(((Purse) purse).count());

			if (((Purse) purse).getCapacity() < ((Purse) purse).count()) {
				statusLabel.setText("EXCEED");
				statusLabel.setForeground(Color.RED);
				statusBar.setForeground(Color.RED);
			} else if (((Purse) purse).getCapacity() == ((Purse) purse).count()) {
				statusLabel.setText("FULL");
				statusLabel.setForeground(Color.RED);
				statusBar.setForeground(Color.RED);
			} else if (((Purse) purse).count() <= 0) {
				statusLabel.setText("EMPTY");
				statusLabel.setForeground(Color.ORANGE.darker());
				statusBar.setForeground(Color.ORANGE.darker());
			} else {
				statusLabel.setText(Integer.toString(((Purse) purse).count()));
				statusLabel.setForeground(Color.GREEN.darker());
				statusBar.setForeground(Color.GREEN.darker());
			}
		}

		if (optArg != null) {
			System.out.println(optArg);
		}
	}

	/**
	 * Initialize all UI components of PurseObserver.
	 */
	private void initUIComponents() {
		balanceFrame = new JFrame("Purse Balance");
		statusFrame = new JFrame("Purse Status");
		balanceLabel = new JLabel("0.0");
		statusLabel = new JLabel("EMPTY");
		statusBar = new JProgressBar();

		balanceFrame.setLayout(new BorderLayout());
		statusFrame.setLayout(new BorderLayout());

		balanceFrame.add(balanceLabel, BorderLayout.EAST);
		balanceFrame.add(new JLabel("THB"), BorderLayout.WEST);
		statusFrame.add(statusLabel, BorderLayout.NORTH);
		statusFrame.add(statusBar, BorderLayout.SOUTH);
		statusBar.setMaximum(0);

		balanceFrame.setAlwaysOnTop(true);
		statusFrame.setAlwaysOnTop(true);

		balanceLabel.setFont(new Font("Sans Serif", Font.PLAIN, 20));
		statusLabel.setFont(new Font("Sans Serif", Font.BOLD, 15));
		
		statusLabel.setForeground(Color.ORANGE.darker());
		statusBar.setForeground(Color.ORANGE.darker());

		balanceFrame.setLocation(50, 100);
		statusFrame.setLocation(50, 200);

		balanceFrame.setPreferredSize(new Dimension(300, 70));
		statusFrame.setPreferredSize(new Dimension(300, 70));

		balanceFrame.pack();
		statusFrame.pack();
	}
}

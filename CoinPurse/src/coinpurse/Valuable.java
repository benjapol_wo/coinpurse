package coinpurse;

/**
 * For valuable stuff that its value can be converted into currency. This
 * interface makes sure we can get the value from objects that realizes this
 * interface.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.2.10
 */
public interface Valuable extends Comparable<Valuable> {
	/**
	 * Make sure that we can get the value from any Valuables
	 * 
	 * @return the real value of a Valuable in currency
	 */
	public double getValue();

	/**
	 * Make sure that we can get the String representation of any Valuables
	 * 
	 * @return a String representation of the Valuable
	 */
	public String toString();

	/**
	 * Make sure that we can test if two Valuables are equal
	 * 
	 * @return true if two Valuables are equal, false otherwise
	 */
	public boolean equals(Object obj);
}

package coinpurse;

import java.util.Comparator;

/**
 * A custom-made ValueComparator that realizes java.util.Comparator.
 * Specifically built to compare Valuables.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.2.10
 */
public class ValueComparator implements Comparator<Valuable> {

	/**
	 * Compare the value of two Valuables
	 * 
	 * @param arg0
	 *            is the first Valuable
	 * @param arg1
	 *            is the second Valuable
	 * @return true if the value of the first Valuable is equals to the value of
	 *         the second Valuable, false otherwise
	 */
	public int compare(Valuable arg0, Valuable arg1) {
		return (int) Math.signum(arg0.getValue() - arg1.getValue());
	}
}

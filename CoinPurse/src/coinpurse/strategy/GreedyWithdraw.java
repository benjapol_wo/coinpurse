package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;

import coinpurse.Valuable;
import coinpurse.ValueComparator;

/**
 * A WithdrawStrategy that will try to withdraw from the purse using Greedy's
 * algorithm. This strategy cannot withdraw from the purse in certain
 * circumstances, such as when trying to withdraw(6) from a Valuable array of
 * {5, 2, 2, 2}, this strategy yield return null as a result.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.2.17
 */
public class GreedyWithdraw implements WithdrawStrategy {
	/**
	 * Try to withdraw from the purse by using Greedy's algorithm.
	 */
	@Override
	public Valuable[] withdraw(double amount, ArrayList<Valuable> valuables) {

		int totalWithdraw = 0, listIndex = 0;
		Collections.sort(valuables, new ValueComparator());
		Collections.reverse(valuables);
		ArrayList<Valuable> withdrawArray = new ArrayList<Valuable>();

		while (totalWithdraw < amount && listIndex < valuables.size()) {
			Valuable temp_valuable = valuables.get(listIndex);
			if (temp_valuable.getValue() > amount - totalWithdraw) {
				listIndex++;
				continue;
			}
			if (temp_valuable.getValue() <= amount - totalWithdraw) {
				withdrawArray.add(temp_valuable);
				totalWithdraw += temp_valuable.getValue();
			}
		}

		if (totalWithdraw != amount) {
			return null;
		} else {
			return withdrawArray.toArray(new Valuable[0]);
		}
	}
}

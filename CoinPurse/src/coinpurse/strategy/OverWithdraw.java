package coinpurse.strategy;

import java.util.ArrayList;

import coinpurse.Valuable;

/**
 * A withdraw strategy that will try to withdraw from the Purse the closest
 * larger amount that is possible.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.2.23
 */
public class OverWithdraw implements WithdrawStrategy {

	/**
	 * Withdraw from the Purse the closest larger amount. The amount withdrawn
	 * will only be equal or larger than the requested amount.
	 */
	@Override
	public Valuable[] withdraw(double amount, ArrayList<Valuable> valuables) {
		if (valuables.size() <= 0) {
			return null;
		}
		int totalValue = 0;
		for (Valuable val : valuables) {
			totalValue += val.getValue();
		}
		if (amount > totalValue) {
			return null;
		}
		Valuable[] withdrawCache = null;
		for (double i = amount; i <= totalValue && withdrawCache == null; i++) {
			withdrawCache = new RecursiveWithdraw().withdraw(i, valuables);
		}
		return withdrawCache;
	}

}

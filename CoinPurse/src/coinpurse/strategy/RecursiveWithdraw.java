package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;

import coinpurse.Valuable;

/**
 * A WithdrawStrategy that will try to withdraw from the Purse using recursive
 * method.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.2.22
 */
public class RecursiveWithdraw implements WithdrawStrategy {

	/**
	 * Try to withdraw from the Purse by using recursive method.
	 */
	@Override
	public Valuable[] withdraw(double amount, ArrayList<Valuable> valuables) {
		Collections.sort(valuables);
		ArrayList<Valuable> withdrawList = withdrawHelper(valuables.size() - 1,
				valuables, amount);
		if (valuables.size() == 0) {
			return new Valuable[0];
		}
		if (withdrawList != null) {
			return withdrawList.toArray(new Valuable[0]);
		}
		return null;
	}

	/**
	 * *HELPER METHOD*
	 * 
	 * Recursively try to withdraw from the Purse. Starting from the last index
	 * of the Valuable(s) list.
	 * 
	 * @param index
	 *            - the last index of valuables
	 * @param valuables
	 *            - the list of Valuable(s) in the Purse
	 * @param amount
	 *            - withdrawal amount
	 * @return an ArrayList of Valuable(s) that are to be removed from the purse
	 *         if the withdrawal is possible. Will return null if unable to
	 *         withdraw.
	 */
	private ArrayList<Valuable> withdrawHelper(int index,
			ArrayList<Valuable> valuables, double amount) {
		if (index < 0) {
			return null;
		}
		if (amount - valuables.get(index).getValue() == 0) {
			ArrayList<Valuable> returnList = new ArrayList<Valuable>();
			returnList.add(valuables.get(index));
			return returnList;
		}
		ArrayList<Valuable> returnList = withdrawHelper(index - 1, valuables,
				amount - valuables.get(index).getValue());
		if (returnList != null) {
			returnList.add(valuables.get(index));
		} else {
			returnList = withdrawHelper(index - 1, valuables, amount);
		}
		return returnList;
	}
}

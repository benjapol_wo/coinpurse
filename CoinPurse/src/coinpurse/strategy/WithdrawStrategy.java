package coinpurse.strategy;

import java.util.ArrayList;

import coinpurse.Valuable;

/**
 * Interface that will be realized by withdraw strategies and guarantee that
 * they all have the same format of withdraw() method when called by the Purse.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.2.22
 */
public interface WithdrawStrategy {
	/**
	 * Try to withdraw from the purse.
	 * 
	 * @param amount
	 *            - the total value of Valuable(s) that you want to withdraw
	 *            from the Purse
	 * @param valuables
	 *            - the list of Valuable(s) in the Purse.
	 * @return Valuable array of Valuable(s) that are to be withdrawn from the
	 *         Purse. Use this to return value to remove Valuable(s) from the
	 *         Purse. Will return null if unable to withdraw.
	 */
	public Valuable[] withdraw(double amount, ArrayList<Valuable> valuables);
}

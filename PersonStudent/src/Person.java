
/**
 * A simple model for a person with a name.
 * 
 * @author Benjapol Worakan 5710546577
 */
public class Person {
	/** the person's name. may contain spaces. */
	private String name;
	
	/**
	 * Initialize a new Person object.
	 * @param name is the name of the new Person
	 */
	public Person(String name) {
		this.name = name;
	}
	
	/**
	 * Get the person's name.
	 * @return the name of this person
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Compare person's by name.
	 * They are equal if the name matches.
	 * @param other is another Person to compare to this one.
	 * @return true if the name is same, false otherwise.
	 */
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (other.getClass() != this.getClass()) {
			return false;
		}
		if (name.equalsIgnoreCase(((Person) other).name)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Get a string representation of this Person.
	 */
	public String toString() {
		return "Person " + name;
	}
}



/**
 * A simple model for a Student with name and id.
 * inherits from Person class.
 * 
 * @author Benjapol Worakan
 */
public class Student extends Person {
	private long id;
	
	/**
	 * Initialize a new Student object.
	 * @param name is the name of the new Student
	 * @param id is the id of the Student
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}
	/**
	 * Compare students by name and id.
	 * They are equal if their name and id match.
	 * @param other is another Student to compare with this one.
	 * @return true if name and id are same, false otherwise.
	 */
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (other.getClass() != this.getClass()) {
			return false;
		}
		if (getName().equals(((Student) other).getName()) && (id == ((Student) other).id)) {
			return true;
		}
		return false;
	}
}
